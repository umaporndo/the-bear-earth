<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Travel Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index'  => 'The bear earth',
        'detail' => 'The bear earth',
    ],

    'page_title' => [
        'index'  => 'The bear earth',
        'detail' => ':earth_title | The bear earth',
        'menu'   => 'The bear earth | :menu_title',
    ],

    'page_description' => [
        'index'  => 'The bear earth',
        'detail' => 'The bear earth :earth_description',
        'menu'   => 'The bear earth | :menu_title',
    ],

    'page_keyword' => [
        'index'  => 'The bear earth',
        'detail' => 'The bear earth :earth_keyword',
        'menu'   => 'The bear earth | :menu_title',
    ],

    'og_title' => [
        'index'  => 'The bear earth',
        'detail' => ':earth_title | The bear earth',
        'menu'   => 'The bear earth | :menu_title',
    ],

    'og_description' => [
        'index'  => 'The bear earth',
        'detail' => ':earth_description',
        'menu'   => 'The bear earth | :menu_title',
    ],

    'og_keyword' => [
        'index'  => 'The bear earth',
        'detail' => ':earth_keyword',
        'menu'   => 'The bear earth | :menu_title',
    ],

    'og_url' => [
        'index'  => route( 'earth.index' ),
        'detail' => route( 'earth.detail', [ 'slug' => ':slug', 'id' => ':earth_id' ] ),
        'menu'   => route( 'earth.menu', [ 'slug' => ':slug', 'menuID' => ':menu_id' ] ),
    ],

];
