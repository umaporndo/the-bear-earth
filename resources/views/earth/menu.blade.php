@extends('layouts.app')

@section('page-title', __('earth.page_title.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name ,
        ]))
@section('page-description', __('earth.page_description.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name ,
        ]))
@section('page-keyword', __('earth.page_keyword.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name ,
        ]))

@section('og-image', asset(config('images.open_graph.default_image')))
@section('og-title', __('earth.og_title.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name,
        ]))
@section('og-description', __('earth.og_description.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name,
        ]))
@section('og-keyword', __('earth.og_keyword.menu',[
            'menu_title' => $contentList[0]['Menu']->menu_name,
        ]))
@section('og-url', __('earth.og_url.menu',[
            'menu_id' => $contentList[0]['Menu']->id,
            'slug' => str_replace(' ', '-', $contentList[0]['Menu']->menu_name),
        ]) )

@section('content')
    <div class="content-desktop">
    </div>
    <div>
        <div class="container content-detail" style="margin-top:70px;">
            @if($contentList->total() !==  0)
                <h1>{{ $contentList[0]['Menu']->menu_name }}</h1>
                <div id="content-list-box">
                    @include('earth.menu_list')

                </div>
            @else
                <h3>Empty Content</h3>
            @endif

        </div>
    </div>
@endsection
