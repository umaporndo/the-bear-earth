@extends('layouts.app')

@section('page-title', __('earth.page_title.detail', [
            'earth_title' => isset($contentDetail['data'][0]->title) ? $contentDetail['data'][0]->title : '',
        ]))
@section('page-description', __('earth.page_description.detail', [
            'earth_description' =>isset($contentDetail['data'][0]->description) ? $contentDetail['data'][0]->description : '' ,
        ]))
@section('page-keyword', __('earth.page_keyword.detail',[
            'earth_keyword' => isset($contentDetail['data'][0]->keyword) ? $contentDetail['data'][0]->description : '',
        ]))

@section('og-image', 'data:image/png;base64,'.$mainImage)
@section('og-title', __('earth.og_title.detail', [
            'earth_title' => isset($contentDetail['data'][0]->title) ? $contentDetail['data'][0]->title : '',
        ] ))
@section('og-description', __('earth.og_description.detail', [
            'earth_description' => isset( $contentDetail['data'][0]->description ) ? $contentDetail['data'][0]->description : '',
        ] ))
@section('og-keyword', __('earth.og_keyword.detail',[
            'earth_keyword' => isset( $contentDetail['data'][0]->keyword ) ? $contentDetail['data'][0]->keyword : '',
        ]))
@section('og-url', __('earth.og_url.detail',[
            'earth_id' => isset($contentDetail['data'][0]->id) ? $contentDetail['data'][0]->id : '',
            'slug' => str_replace(" ", "-", isset( $contentDetail['data'][0]->title ) ? $contentDetail['data'][0]->title : '')
        ]) )

@section('og-sitename', __('earth.page_title.index') )

@section('autopilot-script')
    <!-- Autopilot thebearearth capture code -->
    <script>    window.ap3c = window.ap3c || {};
		var ap3c            = window.ap3c;
		ap3c.cmd            = ap3c.cmd || [];
		ap3c.cmd.push( function(){
			ap3c.init( 'YNweyh6zCBzay-eldGhlYmVhcnRyYXZlbA', 'https://capture-api.autopilotapp.com/' );
			ap3c.track( { v: 0 } );
		} );
		var s, t;
		s      = document.createElement( 'script' );
		s.type = 'text/javascript';
		s.src  = 'https://s.autopilotapp.com/app.js';
		t      = document.getElementsByTagName( 'script' )[0];
		t.parentNode.insertBefore( s, t );</script>
@endsection

@section('content')

    <div class="container pt-1">
        <div>
            <img src="data:image/png;base64,{{ $mainImage }}" class="w-100" alt="">
        </div>
        <div class="container content-detail-660 pt-2" style="margin-top:30px">
            <div class="row pt-2">
                <div class="col text-left">
                    BY {{ $contentDetail['data'][0]['Author'] ? $contentDetail['data'][0]['Author']->name : '' }}</div>
            </div>

            <div class="row pt-2">
                <div class="col">
                    <h1>{{ $contentDetail['data'][0]->title }}</h1>
                    {!! $contentDetail['newContent'] !!}
                </div>
            </div>
        </div>

        @include('earth.footer')
        @include('earth.more')

    </div>
@endsection