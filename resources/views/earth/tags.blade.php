@extends('layouts.app')

@section('page-title', __('earth.page_title.menu',[
            'menu_title' => '',
        ]))
@section('page-description', __('earth.page_description.menu',[
            'menu_title' => '',
        ]))
@section('page-keyword', __('earth.page_keyword.menu',[
            'menu_title' => '',
        ]))

@section('og-image', asset(config('images.open_graph.default_image')))
@section('og-title', __('earth.og_title.menu',[
            'menu_title' => '',
        ]))
@section('og-description', __('earth.og_description.menu',[
            'menu_title' => '',
        ]))
@section('og-keyword', __('earth.og_keyword.menu',[
            'menu_title' => '',
        ]))
@section('og-url', __('earth.og_url.menu',[
            'menu_id' => '',
            'slug' => ''
        ]) )

@section('content')
    <div class="content-desktop">
    </div>
    <div>
        <div class="container content-detail" style="margin-top:70px;">
            @if($contentList->total() !==  0)
                <h1>{{ $slug }}</h1>
                <div id="content-list-box">
                    @include('earth.tags_list')
                </div>
            @else
                <h3>Empty Content</h3>
            @endif

        </div>
    </div>
@endsection
